require.config({
    paths: {
        react: '/static/app/Sample-EA-workflow/react-ui/node_modules/react/umd/react.development',
        'react-dom': '/static/app/Sample-EA-workflow/react-ui/node_modules/react-dom/umd/react-dom.development',
        Index: '/static/app/Sample-EA-workflow/react-ui/dist/bundle',
    }
});

require([
    'underscore',
    'backbone',
    'splunkjs/mvc',
    'jquery',
    'react',
    'react-dom',
    'Index',
    
], function(_, Backbone, mvc, $, React, ReactDOM,Index) {
    console.log(Index.default);
    ReactDOM.render(
        React.createElement(Index.default),
        document.getElementById('root')
    );
});