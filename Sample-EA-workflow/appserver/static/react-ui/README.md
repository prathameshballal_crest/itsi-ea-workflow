## Add this stanza in Splunk\etc\system\local\server.conf

[httpServer]
crossOriginSharingPolicy = http://localhost:3000, http://Splunk_vm_ip:8001, http://127.0.0.1:8001
crossOriginSharingHeaders= X-Splunk-Form-Key, Accept ,Content-Type, User-Agent,Referer