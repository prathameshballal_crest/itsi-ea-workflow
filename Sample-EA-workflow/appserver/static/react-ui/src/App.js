
import './App.css';
import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import Navbar from './components/Navbar';
import { Routes, Route, Navigate } from "react-router-dom";
import Login from './components/Login';
import { useDispatch, useSelector } from 'react-redux';
import Home from './components/Home';
import NeapLister from './components/NEAP/NeapLister';
import { AuthActions, NeapActions } from './store';
import NeapCreateForm from './components/NEAP/NeapCreateForm';
import NeapEditForm from './components/NEAP/NeapEditForm';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactDOM from 'react-dom';
const path = require('path');
import RouteConst from '../routes_const.json'

function App() {
  const dispatch = useDispatch();
  const AuthStatus = useSelector((state) => state.Auth.AuthStatus)

  const authToken = JSON.parse(localStorage.getItem("authToken"))
  //Checking If Token is already exits. If so then Login
  if (authToken) {
    dispatch(AuthActions.login(authToken))
  }
  const editPolicyExits = localStorage.getItem("policy")
  if (editPolicyExits) {
    dispatch(NeapActions.edit(JSON.parse(editPolicyExits)))
  }

  return (
    <>
   
      <Navbar />

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />

      <div className="App App-header">

        <Routes>
          <Route path={RouteConst["home"]} element={<NeapLister/>} />
          {/* <Route path="event-search" element={!AuthStatus ? <Navigate replace to="/login" /> : <CSListerPage />} /> CS Lister Page */}
          <Route path="/neap" element={!AuthStatus ? <Navigate replace to="/login" /> : <NeapLister />} />           {/* NEAP Lister Page*/}
          <Route path="/en-US/app/demoForReact/homepage/login" element={<Login />} />                                                                {/* Login Page*/}
          <Route path={RouteConst["create-neap"]} element={<NeapCreateForm />} />  {/* NEAP Create Page*/}
          <Route path={RouteConst["edit_req"]} element={<NeapEditForm />} />  {/* NEAP Create Page*/}
          {/* <Route path="/correlation/create" element={!AuthStatus ? <Navigate replace to="/login" /> : <EventForm />} />  CS Create Page */}
        </Routes>

      </div>
   
    </>
  );
}

export default App;
