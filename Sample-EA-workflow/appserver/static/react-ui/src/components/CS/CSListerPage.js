import { useState } from "react";
import { useNavigate } from "react-router-dom";
import NeapList from "../NEAP/NeapList";

function CSListerPage() {

    const initTempCS = [
        {
            id: 1,
            neapTitle: "demo CS 1",
        },
        {
            id: 2,
            neapTitle: "demo CS 2",
        },
        {
            id: 3,
            neapTitle: "demo CS 3",
        },
        {
            id: 4,
            neapTitle: "demo CS 4",
        },
        {
            id: 5,
            neapTitle: "demo CS 5",
        }
    ]

    const [tempCS, setTempNeap] = useState(initTempCS);
    const navigate = useNavigate();

    function onDeleteHandler(cs_id) {
        // Make API CALL to Delete data from database.
        setTempNeap(tempCS.filter((item) => item.id !== cs_id))
    }
    function onEditHandler(cs_id) {
        // Make Req. to server with Policy id, After getting data Display it to Edit page
    }

    function onCreateHandler() {
        navigate("/correlation/create")
    }
    return (
        <div className="container">
            <div className="d-flex flex-row-reverse">
                <button className="btn btn-primary" onClick={onCreateHandler}>Create Correlation Searches</button>
            </div>
            <table className="table text-white">
                <thead >
                    <tr>
                        <th scope="col">Title</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {tempCS.map((item) => {
                        return <NeapList key={item.id} policy={item} onDelete={onDeleteHandler} onEdit={onEditHandler} />
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default CSListerPage