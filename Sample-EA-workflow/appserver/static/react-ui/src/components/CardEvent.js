import React from 'react';
function CardEvent(props) {

    function headerColor() {
        if (props.eventData.status === "Resolved" || props.eventData.status === "Solved")
            return 'text-success'
        if (props.eventData.status === "Inprogress")
            return 'text-danger'
    }
    return (
        <div className="card m-2 text-dark">
            <div className={`card-header ${headerColor()} fw-bold`} >
                {props.eventData.status}
            </div >
            <div className="card-body" >
                <blockquote className="blockquote mb-0" >
                    <p>{props.eventData.title}</p>
                    <footer className="blockquote-footer" > {props.eventData.author}</footer >
                </blockquote >
            </div >
        </div >
    );
}

export default CardEvent