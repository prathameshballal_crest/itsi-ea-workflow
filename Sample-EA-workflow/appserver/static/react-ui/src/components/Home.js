import CardEvent from "./CardEvent";
import React from 'react';
const testEvents = [
    {
        id: 1,
        title: "Event title 1",
        author: "Author 1",
        fileSource: "Source 1",
        logLevel: "Info",
        status: "Inprogress"
    },
    {
        id: 2,
        title: "Event title 2",
        author: "Author 2",
        fileSource: "Source 2",
        logLevel: "Info",
        status: "Resolved"
    },
    {
        id: 3,
        title: "Event title 3",
        author: "Author 3",
        fileSource: "Source 1",
        logLevel: "Critical",
        status: "Solved"
    }
]


function Home() {
    return (
        <div className="container ">
            {
                testEvents.map((event) => {
                    return <CardEvent key={event.id} eventData={event} />
                })
            }
        </div>
    );
}

export default Home