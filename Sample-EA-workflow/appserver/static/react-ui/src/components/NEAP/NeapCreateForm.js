import React from 'react'
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Selection from "../Selection";
import { MdCancel } from "react-icons/md";
import FilterComponent from "./FilterComponent";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import Const from '../../../routes_const.json'

const BreakingCriteria = ["5 Event/Episode", "10 Events/Episodes"];
const filteringCriteria = ["matches", "does not match", "greater than", "greater than or equal to", "less than", "less than or equal to"];

function NeapCreateForm() {
    const [data, setData] = useState([]);   // For Filter input
    const [addRuleORCount, setAddRuleORCount] = useState(0);  // count of OR inputs

    const [andConditions, setAndConditions] = useState([]); // For AND inputs

    const [breakSplit, setBreakSplit] = useState({ breakBy: BreakingCriteria[0], splitBy: "" }); // For Break, Split inputs

    const [titleDesc, setTitleDesc] = useState({ title: "", description: "" })

    const [error, setError] = useState({
        generalError: "",
        success: null,
    });

    const [loading, setIsLoading] = useState(false);

    const navigate = useNavigate();

    function changeHandler(event, id, isAnd = false) {
        setError({
            generalError: "",
            success: null,
        }); // Resetting error when user is changing input.

        if (isAnd) {
            setAndConditions(() => {
                let newFields = [...andConditions]; // Copy Data
                newFields.find((item) => item.id === id)[event.target.name] =
                    event.target.value; // Updating Data
                return newFields; // Returning Updated data
            });
        } else {
            setData(() => {
                let newFields = [...data]; // Copy Data
                newFields.find((item) => item.id === id)[event.target.name] =
                    event.target.value; // Updating Data
                return newFields; // Returning Updated data
            });
        }
    }

    function titleDescChangeHandler(event) {
        setError({
            generalError: "",
            success: null,
        }); // Resetting error when user is changing input.

        const name = event.target.name;
        const value = event.target.value

        setTitleDesc({ ...titleDesc, [name]: value })

    }

    function breakSplitHandler(e) {
        const fieldName = e.target.name;
        const value = e.target.value;
        setBreakSplit({ ...breakSplit, [fieldName]: value });
    }

    // set Previous States after Re-render
    useEffect(() => {
        setData((prevState) => {
            return [
                ...prevState,
                {
                    filterBy: "",
                    id: addRuleORCount,
                    filterBySecond: "",
                    filterCriteria: filteringCriteria[0],
                },
            ];
        });
        setAddRuleORCount((count) => count + 1);
        setAndConditions((prevState) => {
            return [
                ...prevState,

            ];
        });
    }, []);

    // add OR Rule function
    function addRuleORHandler() {
        setData((prevState) => {
            return [
                ...prevState,
                {
                    filterBy: "",
                    id: addRuleORCount,
                    filterBySecond: "",
                    filterCriteria: filteringCriteria[0],
                },
            ];
        });
        setAddRuleORCount(addRuleORCount + 1);
    }

    // remove AND,OR Rule function
    function removeORHandler(e, id, isAnd = false) {
        if (isAnd) {
            setAndConditions((prevState) => {
                let newData = prevState.filter((item) => item.id !== id);
                return newData;
            });
            setAddRuleORCount(addRuleORCount - 1);
        }
        else {

            setAndConditions((prevState) => {
                let newData = prevState.filter((item) => item.related_id !== id);
                return newData;
            });

            setAddRuleORCount((addRuleORCount) => addRuleORCount - 1);

            setData((prevState) => {


                let newData = prevState.filter((item) => item.id !== id);
                return newData;
            });
            setAddRuleORCount((addRuleORCount) => addRuleORCount - 1);
        }

    }

    function addAndRule(e, id) {

        // setting has_AND_conditions = true
        setData(() => {
            let newFields = [...data]; // Copy Data
            newFields.find((item) => item.id === id)["has_AND_conditions"] = true;
            return newFields; // Returning Updated data
        });

        setAndConditions((prevState) => {
            return [
                ...prevState,
                {
                    filterBy: "",
                    id: addRuleORCount,
                    related_id: id,
                    filterBySecond: "",
                    filterCriteria: filteringCriteria[0],
                },
            ];
        });
        setAddRuleORCount(addRuleORCount + 1);
    }

    function submitHandler(event) {
        event.preventDefault();

        if(!titleDesc.title)
        {
            setError({...error,title:"Title Field Is Required.",success: false})
            return
        }
        if( data.length ===0 || !data[0].filterBy || !data[0].filterBySecond ){
            setError({...error, atLeastOneFilter:"At Least One Filter is Required.",success: false})
            return
        }

        // Adding `and_rules = []` Field where there is AND conditions.
        let keyAdd = data.map((rule) => {

            if (rule.has_AND_conditions) {
                const test = rule.and_rules = []
                return { ...rule, ...test }
            }
            else {
                return rule
            };

        })

        // adding AND rules to Related Fields. ex. Field 1 has 2 AND condition so it will get appended in `and_rules` array.
        keyAdd = keyAdd.filter((item) => {
            if (item.has_AND_conditions) {
                return andConditions.map((andItem) => {
                    if (andItem.related_id === item.id)
                        return { ...item, ...item.and_rules.push(andItem) }
                })
            }
            else {
                return item
            };
        })
        const finalJsonData = {
            title: titleDesc.title,
            description: titleDesc.description,
            "filter_criteria": {
                "condition": "OR",
                "rules": keyAdd
            },
            "split_criteria": breakSplit.splitBy.split(','),
            "breaking_criteria": breakSplit.breakBy

        }
        setIsLoading(true);
        axios.post(Const['create_neap_req'], JSON.stringify(finalJsonData), {
            auth: {
                username: Const['username'],
                password: Const['password']
            }
        }).then((res) => {
            setIsLoading(false);
            if (res.status === 200) {
                toast.success('NEAP Created!', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
            setTimeout(() => {
                navigate(-1)
            }, 3000);

        }).catch((error) => {
            if (error.toString().includes("status code")) {
                toast.error(error.toString() ?? "Something Went Wrong!", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }

            setIsLoading(false);
        })



    }

    return (

        <div
            className="container  d-flex justify-content-center align-items-center"
            onSubmit={submitHandler}
        >
            <form className="col col-md-6">
                <p className="text-center h3 text-white m-3">
                    Notable Event Aggregation Policy
                </p>

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="title">Title</label>
                    <input
                        id="title"
                        type="text"
                        className="form-control"
                        name="title"
                        value={titleDesc.title}
                        placeholder="Enter Title"
                        onChange={titleDescChangeHandler}
                    />
                    {(error.title!=="" && error.success===false) && <p className='text-danger'>{error.title}</p>}
                    <label htmlFor="description">Description</label>
                    <input
                        id="description"
                        type="text"
                        className="form-control"
                        name="description"
                        value={titleDesc.description}
                        placeholder="Enter description"
                        onChange={titleDescChangeHandler}
                    />
                </div>

                <input
                    type="button"
                    className="btn btn-link"
                    onClick={addRuleORHandler}
                    value="Add Rule (OR)"
                />

                {data.map((item) => {
                    return (
                        <div className="form-group row" key={item.id}>
                            <label htmlFor="filterBy">Filter By</label>
                            <div className="d-flex bg-secondary p-2">
                                <input
                                    type="text"
                                    id="filterBy"
                                    className="form-control me-1 col"
                                    name="filterBy"
                                    value={item.filterBy || ""}
                                    placeholder="Enter Field"
                                    onChange={(e) => changeHandler(e, item.id)}
                                />
                                <Selection
                                    className="col"
                                    id={item.id}
                                    options={filteringCriteria}
                                    name="filterCriteria"
                                    onSelectionChange={changeHandler}
                                />
                                {/** TODO: Changing in State Caused Error In selection */}

                                <input
                                    type="text"
                                    id="filterBy"
                                    className="form-control ms-1 col"
                                    name="filterBySecond"
                                    value={item.filterBySecond || ""}
                                    placeholder="Enter Field"
                                    onChange={(e) => changeHandler(e, item.id)}
                                />

                                <button
                                    type="button"
                                    className="btn btn-link"
                                    onClick={(e) => removeORHandler(e, item.id)}
                                >
                                    <MdCancel color="red" />
                                </button>

                                <button
                                    type="button"
                                    className="btn btn-link"
                                    style={{ color: "blue" }}
                                    onClick={(e) => addAndRule(e, item.id)}
                                >
                                    AND
                                </button>
                            </div>

                            {item.has_AND_conditions === true
                                ? andConditions.map((andItem) => {
                                    return (
                                        andItem.related_id === item.id && (
                                            <FilterComponent
                                                key={andItem.id}
                                                id={andItem.id}
                                                changeHandler={changeHandler}
                                                filteringCriteria={filteringCriteria}
                                                item={andItem}
                                                removeORHandler={removeORHandler}
                                            />
                                        )
                                    );
                                })
                                : ""}

                            <small className="text-info h-6">
                                Ex. You can match on any fields in the event.
                            </small>
                        </div>
                    );
                })}
             {(error.atLeastOneFilter!=="" && error.success===false) && <p className='text-danger'>{error.atLeastOneFilter}</p>}

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="splitBy">Split By</label>
                    <input
                        id="splitBy"
                        type="text"
                        className="form-control"
                        name="splitBy"
                        value={breakSplit.splitBy}
                        placeholder="Enter Field"
                        onChange={breakSplitHandler}
                    />
                    <small className="text-info h-6">
                        Ex. You can splits events by Host, Source type, Severity.
                    </small>
                </div>

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="title">Break Episode</label>
                    <Selection
                        options={BreakingCriteria}
                        name="breakBy"
                        onSelectionChange={breakSplitHandler}
                    />
                </div>

                <button
                    type="submit"
                    className="btn btn-primary form-control bg-shadow-color mt-1"
                >
                    {loading && <div className="spinner-border small " role="status">
                        <span className="sr-only"></span>
                    </div>}

                    {!loading && "Create NEAP"}
                </button>
            </form>
        </div >

    );
}

export default NeapCreateForm;