import { combineReducers, configureStore, createSlice } from '@reduxjs/toolkit'


const initialState = {
    AuthStatus: false
}
const Auth = createSlice({
    name: 'Auth',
    initialState: initialState,
    reducers: {
        login: (state, action) => {
            if (action.payload.email === "admin@admin.com" && action.payload.password === "admin123") {
                localStorage.setItem("authToken", JSON.stringify({ email: action.payload.email, password: action.payload.password, token: true }))
                state.AuthStatus = true
            }
            else {
                state.AuthStatus = false
                localStorage.removeItem("authToken");
            }
        },
        logout: (state) => {
            state.AuthStatus = false
            localStorage.removeItem("authToken");
        }
    }
})

const initialStateEdit = {
    editNeap: {}
}
const editNeapObject = createSlice({
    name: 'editNeap',
    initialState: initialStateEdit,
    reducers: {
        edit: (state, action) => {
            state.editNeap = action.payload
            localStorage.setItem("policy", JSON.stringify(action.payload))
        }
    }
})

const reducer = combineReducers({
    Auth: Auth.reducer,
    Edit: editNeapObject.reducer,
})
const store = configureStore({
    reducer: reducer
});

export const AuthActions = Auth.actions;
export const NeapActions = editNeapObject.actions;
export default store