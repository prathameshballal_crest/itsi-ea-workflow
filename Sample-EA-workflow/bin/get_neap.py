# C:\Program Files\Splunk\Python-3.7\Lib\site-packages\splunk\persistconn
from splunk.persistconn.application import PersistentServerConnectionApplication


from splunklib.client import connect


from utils import parse

import json
import sys


class GetNeapObj(PersistentServerConnectionApplication):
    def __init__(self, _command_line, _command_arg):
        super(PersistentServerConnectionApplication, self).__init__()

    # Handle a syncronous from splunkd.
    def handle(self, in_string):
        opts = parse(sys.argv[1:], {}, ".splunkrc")
        opts.kwargs["username"] = "username"
        opts.kwargs["password"] = "password"
        opts.kwargs["owner"] = "Prathamesh Ballal, Nachiketa Bhoraniya, Rajdeep Kamai"
        opts.kwargs["app"] = "Sample-EA-workflow"
        print(opts)
        try:
            service = connect(**opts.kwargs)
        except Exception as e:
            print("Error in connect")
            print(e)
        collection_name = "neap_object_test"
        # if the collection is found, print it out
        # if not, then create the collection
        if collection_name in service.kvstore:
            print("Collection %s found!" % collection_name)
        else:
            service.kvstore.create(collection_name)
        collection = service.kvstore[collection_name]
        print("Collection data: %s" % json.dumps(collection.data.query(), indent=1))
        # payload = {"return_object": "json.dumps(collection.data.query(), indent=1)"}
        object_data = json.dumps(collection.data.query(), indent=1)
        return {"payload": json.loads(object_data), "status": 200}

    def handleStream(self, handle, in_string):
        """
        For future use
        """
        raise NotImplementedError("PersistentServerConnectionApplication.handleStream")

    def done(self):
        """
        Virtual method which can be optionally overridden to receive a
        callback after the request completes.
        """
        pass
