import sys, json
from splunklib.client import connect

try:
    from utils import parse
except ImportError:
    raise Exception(
        "Add the SDK repository to your PYTHONPATH to run the examples "
        "(e.g., export PYTHONPATH=~/splunk-sdk-python."
    )


def main():
    # in this example, i’m using a .splunkrc file to pull credentials
    opts = parse(sys.argv[1:], {}, ".splunkrc")
    opts.kwargs["username"] = "username"
    opts.kwargs["password"] = "password"
    opts.kwargs["owner"] = "Prathamesh Ballal, Nachiketa Bhoraniya, Rajdeep Kamai"
    opts.kwargs["app"] = "Sample-EA-workflow"
    try:
        service = connect(**opts.kwargs)
    except Exception as e:
        print("Error in connect")
        print(e)
    collection_name = "neap_object"
    # if the collection is found, print it out
    # if not, then create the collection
    if collection_name in service.kvstore:
        print("Collection %s found!" % collection_name)
    else:
        service.kvstore.create(collection_name)
    collection = service.kvstore[collection_name]
    # print out the data from the collection
    collection.data.insert(
        json.dumps(
            {
                "Task_Name": "Python Task",
                "Task_Description": "This task was created in python.",
                "Status": "InProgress",
                "Estimated_Completion_Date": "October 20th",
                "Notes": "No notes at this time.",
            }
        )
    )
    print("Collection data: %s" % json.dumps(collection.data.query(), indent=1))


if __name__ == "__main__":
    main()
