import { useState } from "react";
import Selection from "../Selection";

const LogLevels = ["Info", "Critical", "Debug", "Warning"]
const Status = ["Inprogress", "Resolved", "Solved"]

function EventForm() {
    const [data, setData] = useState({
        title: "",
        author: "",
        fileSource: "",
        logLevels: LogLevels[0],
        status: Status[0],
    });
    const [error, setError] = useState({
        generalError: "",
        success: null,
    });

    function changeHandler(event) {
        setError({
            generalError: "",
            success: null,
        }); // Resetting error when user is changing input.

        const name = event.target.name;
        const value = event.target.value;
        setData({ ...data, [name]: value });
    }

    function submitHandler(event) {
        event.preventDefault();
        setError({
            generalError: "",
            success: null,
        });
        if (data.title === "" || data.author === "" || data.fileSource === "" || data.logLevels === "" || data.status === "") {
            setError({ ...error, generalError: "Please Enter all Fields." });
        } else {
            setError({ ...error, success: true });// We can do API call Here to send req.
        }
    }

    // For displaying error and Success
    function validateInputs() {
        if (error.success != null && error.success === true) {
            return <p className="text-success m-1">Submission Successful.</p>;
        } else if (error.success != null && error.success === false) {
            return <p className="text-danger m-1">Something went wrong.</p>;
        } else {
            return;
        }
    }

    return (
        <div
            className="container vh-100 d-flex justify-content-center align-items-center"
            onSubmit={submitHandler}
        >
            <form className="col col-md-4">
                <p className="text-center h3 text-white m-3">Correlation Searches</p>
                <div className="form-group">
                    <label htmlFor="title">Title of Event</label>
                    <input
                        type="text"
                        id="title"
                        className="form-control"
                        name="title"
                        value={data.username}
                        placeholder="Title of Event"
                        onChange={changeHandler}
                    />
                </div>
                <div className="form-group mt-2 mb-2">
                    <label htmlFor="author">Author</label>
                    <input
                        id="author"
                        type="text"
                        className="form-control"
                        name="author"
                        value={data.email}
                        placeholder="Author"
                        onChange={changeHandler}
                    />
                </div >

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="fileSource">Source of file</label>
                    <input
                        type="text"
                        id="fileSource"
                        className="form-control"
                        name="fileSource"
                        placeholder="Source of file"
                        value={data.password}
                        onChange={changeHandler}
                    />
                </div >

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="logLevels">Log Level</label>
                    <Selection options={LogLevels} name="logLevels" onSelectionChange={changeHandler} />
                </div >

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="status">Status</label>
                    <Selection options={Status} name="status" onSelectionChange={changeHandler} />
                </div >


                <button
                    type="submit"
                    className="btn btn-primary form-control bg-shadow-color mt-1"
                >
                    Create Search
                </button>
                {
                    error.generalError !== "" && (
                        <p className="text-danger m-1">{error.generalError}</p>
                    )
                }
                {validateInputs()}
            </form >
        </div >
    );
}

export default EventForm;
