import Selection from "../Selection";
import { MdCancel } from 'react-icons/md';
import React from 'react';
function FilterComponent(props) {

    return (


        <div className="d-flex bg-secondary p-2">
            <input
                type="text"
                id="filterBy"
                className="form-control me-1 col"
                name="filterBy"
                value={props.item.filterBy || ""}
                placeholder="Enter Field"
                onChange={(e) => props.changeHandler(e, props.item.id, true)}
            />
            <Selection value={props.item.filterCriteria} isAnd={true} className="col" id={props.id} options={props.filteringCriteria} name="filterCriteria" onSelectionChange={props.changeHandler} />
            {/** TODO: Changing in State Caused Error In selection */}

            <input
                type="text"
                id="filterBy"
                className="form-control ms-1 col"
                name="filterBySecond"
                value={props.item.filterBySecond || ""}
                placeholder="Enter Field"
                onChange={(e) => props.changeHandler(e, props.item.id, true)}
            />


            <button
                type="button"
                className="btn btn-link me-1"
                onClick={(e) => props.removeORHandler(e, props.item.id, true)}
            >
                <MdCancel color="red" />
            </button>
            <span className="col-1"></span>

        </div>



    )
}

export default FilterComponent;