import React from 'react';
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Selection from "../Selection";
import { MdCancel } from "react-icons/md";
import FilterComponent from "./FilterComponent";
import axios from "axios";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";


const BreakingCriteria = ["5 Event/Episode", "10 Events/Episodes"];
const filteringCriteria = [
    "matches",
    "does not match",
    "greater than",
    "greater than or equal to",
    "less than",
    "less than or equal to",
];

function NeapEditForm() {
    const [data, setData] = useState([]); // For Filter input
    const [addRuleORCount, setAddRuleORCount] = useState(0); // count of OR inputs

    const [andConditions, setAndConditions] = useState([]); // For AND inputs

    const [breakSplit, setBreakSplit] = useState({
        breakBy: "",
        splitBy: "",
    }); // For Break, Split inputs

    const [titleDesc, setTitleDesc] = useState({ title: "", description: "" });

    const [error, setError] = useState({
        generalError: "",
        success: null,
    });

    const currentItem = useSelector((state) => state.Edit.editNeap);

    const [loading, setIsLoading] = useState(false);

    const navigate = useNavigate();

    function changeHandler(event, id, isAnd = false) {
        setError({
            generalError: "",
            success: null,
        }); // Resetting error when user is changing input.

        if (isAnd) {
            setAndConditions(() => {
                let newFields = [...andConditions]; // Copy Data
                newFields.find((item) => item.id === id)[event.target.name] =
                    event.target.value; // Updating Data
                return newFields; // Returning Updated data
            });
        } else {
            setData(() => {
                let newFields = [...data]; // Copy Data
                newFields.find((item) => item.id === id)[event.target.name] =
                    event.target.value; // Updating Data
                return newFields; // Returning Updated data
            });
        }
    }

    function titleDescChangeHandler(event) {
        const name = event.target.name;
        const value = event.target.value;

        setTitleDesc({ ...titleDesc, [name]: value });
    }

    function breakSplitHandler(e) {
        const fieldName = e.target.name;
        const value = e.target.value;
        setBreakSplit({ ...breakSplit, [fieldName]: value });
    }

    useEffect(() => {
        // Setting Title, description, Splitby,and breakby.
        setBreakSplit({
            breakBy: currentItem.breaking_criteria,
            splitBy: currentItem.split_criteria.join(),
        });
        setTitleDesc({
            title: currentItem.title,
            description: currentItem.description,
        });

        const rules = currentItem.filter_criteria.rules;
        const array = new Array();

        // Getting AND conditions and saving it in State.
        rules.forEach((item) => {
            if (
                item.hasOwnProperty("has_AND_conditions") &&
                item.has_AND_conditions === true
            ) {
                item.and_rules.forEach((item) => {
                    return array.push({ ...item });
                });
            }
        });
        setAndConditions(array);

        //---------------------------------

        // Deleting AND conditions from main object and then saving it in State.
        let test = rules.map((item, index, array) => {
            const { and_rules, ...newItem } = item;
            return newItem;
        });
        setData(test);
        //Updating Count of fields.
        setAddRuleORCount(test.length + array.length);
    }, []);

    // add OR Rule function
    function addRuleORHandler() {
        setData((prevState) => {
            return [
                ...prevState,
                {
                    filterBy: "",
                    id: addRuleORCount,
                    filterBySecond: "",
                    filterCriteria: filteringCriteria[0],
                },
            ];
        });
        setAddRuleORCount(addRuleORCount + 1);
    }

    // remove AND,OR Rule function
    function removeORHandler(e, id, isAnd = false) {
        if (isAnd) {
            setAndConditions((prevState) => {
                let newData = prevState.filter((item) => item.id !== id);
                return newData;
            });
            setAddRuleORCount(addRuleORCount - 1);
        } else {
            setAndConditions((prevState) => {
                let newData = prevState.filter((item) => item.related_id !== id);
                return newData;
            });

            setAddRuleORCount((addRuleORCount) => addRuleORCount - 1);

            setData((prevState) => {
                let newData = prevState.filter((item) => item.id !== id);
                return newData;
            });
            setAddRuleORCount((addRuleORCount) => addRuleORCount - 1);
        }
    }

    function addAndRule(e, id) {
        // setting has_AND_conditions = true
        setData(() => {
            let newFields = [...data]; // Copy Data
            newFields.find((item) => item.id === id)["has_AND_conditions"] = true;
            return newFields; // Returning Updated data
        });

        setAndConditions((prevState) => {
            return [
                ...prevState,
                {
                    filterBy: "",
                    id: addRuleORCount,
                    related_id: id,
                    filterBySecond: "",
                    filterCriteria: filteringCriteria[0],
                },
            ];
        });
        setAddRuleORCount(addRuleORCount + 1);
    }

    function submitHandler(event) {
        event.preventDefault();

        // Adding `and_rules = []` Field where there is AND conditions.
        let keyAdd = data.map((rule) => {
            if (rule.has_AND_conditions) {
                const test = (rule.and_rules = []);
                return { ...rule, ...test };
            } else {
                return rule;
            }
        });

        // adding AND rules to Related Fields. ex. Field 1 has 2 AND condition so it will get appended in `and_rules` array.
        keyAdd = keyAdd.filter((item) => {
            if (item.has_AND_conditions) {
                return andConditions.map((andItem) => {
                    if (andItem.related_id === item.id)
                        return { ...item, ...item.and_rules.push(andItem) };
                });
            } else {
                return item;
            }
        });
        const finalJsonData = {
            title: titleDesc.title,
            description: titleDesc.description,
            filter_criteria: {
                condition: "OR",
                rules: keyAdd,
            },
            split_criteria: breakSplit.splitBy.split(","),
            breaking_criteria: breakSplit.breakBy,
        };

        setIsLoading(true);
        axios
            .put(
                "https://your_splunk_instance_ip:8089/servicesNS/-/Sample-EA-workflow/update-neap", { "key": currentItem._key, "data": JSON.stringify(finalJsonData) },
                {
                    auth: {
                        username: "username",
                        password: "password",
                    },
                }
            )
            .then((res) => {
                setIsLoading(false);
                localStorage.removeItem("policy"); // removing Policy After Successful Update
                if (res.status === 200) {
                    toast.success('Policy Updated Successfully!', {
                        position: "top-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                    });
                }
                setTimeout(() => {
                    navigate(-1)
                }, 3000);
            })
            .catch((error) => {
                if (error.toString().includes("status code")) {
                    toast.error(error.toString() ?? "Something Went Wrong!", {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
                setIsLoading(false);
            });

        // setError({
        //     generalError: "",
        //     success: null,
        // });

        // if (data.filterBy === "" || data.splitBy === "" || data.breakBy === "") {
        //     setError({ ...error, generalError: "Please Enter all Fields." });
        // } else {
        //     setError({ ...error, success: true }); // We can do API call Here to send req.
        //     // setTimeout(() => {
        //     //     navigate(-1);
        //     // }, 2000);
        // }
    }

    // For displaying error and Success
    function validateInputs() {
        if (error.success != null && error.success === true) {
            return <p className="text-success m-1">Submission Successful.</p>;
        } else if (error.success != null && error.success === false) {
            return <p className="text-danger m-1">Something went wrong.</p>;
        } else {
            return;
        }
    }

    return (
        <div
            className="container  d-flex justify-content-center align-items-center"
            onSubmit={submitHandler}
        >
            <form className="col col-md-6">
                <p className="text-center h3 text-white m-3">
                    Notable Event Aggregation Policy
                </p>

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="title">Title</label>
                    <input
                        id="title"
                        type="text"
                        className="form-control"
                        name="title"
                        value={titleDesc.title}
                        placeholder="Enter Title"
                        onChange={titleDescChangeHandler}
                    />
                    <label htmlFor="description">Description</label>
                    <input
                        id="description"
                        type="text"
                        className="form-control"
                        name="description"
                        value={titleDesc.description}
                        placeholder="Enter description"
                        onChange={titleDescChangeHandler}
                    />
                </div>

                <input
                    type="button"
                    className="btn btn-link"
                    onClick={addRuleORHandler}
                    value="Add Rule (OR)"
                />

                {data.map((item) => {
                    return (
                        <div className="form-group row" key={item.id}>
                            <label htmlFor="filterBy">Filter By</label>
                            <div className="d-flex bg-secondary p-2">
                                <input
                                    type="text"
                                    id="filterBy"
                                    className="form-control me-1 col"
                                    name="filterBy"
                                    value={item.filterBy || ""}
                                    placeholder="Enter Field"
                                    onChange={(e) => changeHandler(e, item.id)}
                                />
                                <Selection
                                    value={item.filterCriteria}
                                    className="col"
                                    id={item.id}
                                    options={filteringCriteria}
                                    name="filterCriteria"
                                    onSelectionChange={changeHandler}
                                />

                                <input
                                    type="text"
                                    id="filterBy"
                                    className="form-control ms-1 col"
                                    name="filterBySecond"
                                    value={item.filterBySecond || ""}
                                    placeholder="Enter Field"
                                    onChange={(e) => changeHandler(e, item.id)}
                                />

                                <button
                                    type="button"
                                    className="btn btn-link"
                                    onClick={(e) => removeORHandler(e, item.id)}
                                >
                                    <MdCancel color="red" />
                                </button>

                                <button
                                    type="button"
                                    className="btn btn-link"
                                    style={{ color: "blue" }}
                                    onClick={(e) => addAndRule(e, item.id)}
                                >
                                    AND
                                </button>
                            </div>

                            {item.has_AND_conditions === true
                                ? andConditions.map((andItem) => {
                                    return (
                                        andItem.related_id === item.id && (
                                            <div className="d-flex bg-secondary p-2 " key={andItem.id}>
                                                <input
                                                    type="text"
                                                    id="filterBy"
                                                    className="form-control me-1 col"
                                                    name="filterBy"
                                                    value={andItem.filterBy || ""}
                                                    placeholder="Enter Field"
                                                    onChange={(e) => changeHandler(e, andItem.id, true)}
                                                />
                                                <Selection
                                                    value={andItem.filterCriteria}
                                                    className="col"
                                                    isAnd={true}
                                                    id={andItem.id}
                                                    options={filteringCriteria}
                                                    name="filterCriteria"
                                                    onSelectionChange={changeHandler}
                                                />
                                                {/** TODO: Changing in State Caused Error In selection */}

                                                <input
                                                    type="text"
                                                    id="filterBy"
                                                    className="form-control ms-1 col"
                                                    name="filterBySecond"
                                                    value={andItem.filterBySecond || ""}
                                                    placeholder="Enter Field"
                                                    onChange={(e) => changeHandler(e, andItem.id, true)}
                                                />

                                                <button
                                                    type="button"
                                                    className="btn btn-link me-1"
                                                    onClick={(e) =>
                                                        removeORHandler(e, andItem.id, true)
                                                    }
                                                >
                                                    <MdCancel color="red" />
                                                </button>
                                                <span className="col-1"></span>
                                            </div>
                                        )
                                    );
                                })
                                : ""}

                            <small className="text-info h-6">
                                Ex. You can match on any fields in the event.
                            </small>
                        </div>
                    );
                })}

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="splitBy">Split By</label>
                    <input
                        id="splitBy"
                        type="text"
                        className="form-control"
                        name="splitBy"
                        value={breakSplit.splitBy}
                        placeholder="Enter Field"
                        onChange={breakSplitHandler}
                    />
                    <small className="text-info h-6">
                        Ex. You can splits events by Host, Source type, Severity.
                    </small>
                </div>

                <div className="form-group mt-2 mb-2">
                    <label htmlFor="title">Break Episode</label>
                    <Selection
                        value={breakSplit.breakBy}
                        options={BreakingCriteria}
                        name="breakBy"
                        onSelectionChange={breakSplitHandler}
                    />
                </div>

                <button
                    type="submit"
                    className="btn btn-primary form-control bg-shadow-color mt-1"
                >
                    {loading && (
                        <div className="spinner-border small " role="status">
                            <span className="sr-only"></span>
                        </div>
                    )}

                    {!loading && "Update NEAP"}
                </button>
                {error.generalError !== "" && (
                    <p className="text-danger m-1">{error.generalError}</p>
                )}
                {validateInputs()}
            </form>
        </div>
    );
}

export default NeapEditForm;
