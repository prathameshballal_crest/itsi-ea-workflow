import React from 'react';
function NeapList(props) {
    function onDeleteHandler() {
        props.onDelete(props.policy._key)
    }
    function onEditHandler() {
        props.onEdit(props.policy)
    }
    return (
        <tr>

            <td>{props.policy.title}</td>
            <td> <button className="btn btn-outline-danger mx-2" onClick={onDeleteHandler}>Delete</button>
                <button className="btn btn-outline-info" onClick={onEditHandler}>Edit</button></td>
        </tr>
    );

}

export default NeapList