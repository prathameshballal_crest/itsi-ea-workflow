import { useEffect, useState } from "react";
import NeapList from "./NeapList";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useDispatch } from "react-redux";
import { NeapActions } from "../../store";
import { toast } from "react-toastify";
import React from 'react';
function NeapLister() {

    const [neapList, setNeapList] = useState([]);
    const [loading, setIsLoading] = useState(false);
    const dispatch = useDispatch()
    const navigate = useNavigate()

    useEffect(() => {

        setIsLoading(true);
        axios.get("https://your_splunk_instance_ip:8089/servicesNS/-/Sample-EA-workflow/get-neap", {
            method: 'get',
            auth: {
                username: "username",
                password: "password"
            }
        })
            .then(function (response) {
                if (response.status === 200)
                    setNeapList(response.data)
                setIsLoading(false);
            })
            .catch(function (error) {

                if (error.status >= 500 && error.status <= 599)
                    alert(error.statusText)
                setIsLoading(false);
            })

    }, [])

    //const [tempNeap, setTempNeap] = useState(initTempNeap);

    function onDeleteHandler(policy_id) {

        setIsLoading(true);
        axios.post("https://your_splunk_instance_ip:8089/servicesNS/-/Sample-EA-workflow/delete-neap?key=" + policy_id, {}, {
            auth: {
                username: "username",
                password: "password"
            },
        }).then((res) => {
            setIsLoading(false);
            if (res.status === 200) {
                setNeapList(neapList.filter((item) => item._key !== policy_id))
                if (res.status === 200) {
                    toast.success('NEAP Deleted!', {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                    });
                }
            }


        }).catch((error) => {
            setIsLoading(false);
            if (error.toString().includes("status code")) {
                toast.error(error.toString() ?? "Something Went Wrong!", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        })
    }

    function onEditHandler(policy) {
        dispatch(NeapActions.edit(policy))
        navigate('/en-US/app/demoForReact/homepage/neap/edit/' + policy._key)
        //save Object in redux and redirect to /neap/edit page with ?key= object key
        // Make Req. to server with Policy id, After getting data Display it to Edit page
    }

    function onCreateHandler() {
        navigate("/en-US/app/demoForReact/homepage/neap/create")
    }
    return (
        <div className="container">
            <div className="d-flex flex-row-reverse">
                <button className="btn btn-primary" onClick={onCreateHandler}>Create NEAP</button>
            </div>
            <table className="table text-white">
                <thead >
                    <tr>
                        <th scope="col">Title</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <>
                        {neapList !== null && neapList.length !== 0 && loading === false && neapList.map((item, index) => {
                            return <NeapList key={item._key} policy={item} onDelete={onDeleteHandler} onEdit={onEditHandler} />
                        })}

                        {(loading === false && neapList.length === 0) && <tr><td colSpan="2">No Policy Exist.</td></tr>}
                    </>
                </tbody>
            </table>

            {loading && <div className="d-flex justify-content-center">
                <div rowSpan="2" className="spinner-border m-2 " role="status">
                    <span className="sr-only"></span>
                </div>
            </div>}

        </div >
    );
}

export default NeapLister;

