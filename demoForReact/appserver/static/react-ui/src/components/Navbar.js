
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { AuthActions } from "../store";
import React from 'react';
function Navbar() {
    const AuthStatus = useSelector((state) => state.Auth.AuthStatus)
    const navigate = useNavigate()
    const dispatch = useDispatch()

    function logoutHandler() {
        dispatch(AuthActions.logout())
        navigate("/login")
    }

    return (
        <nav className="navbar navbar-expand-lg  navbar-dark bg-dark px-3">
            <div className="container-fluid">

                <NavLink className="navbar-brand" to="/">Episode Review</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto mb-2 mb-lg-0" id="navUL">
                        <li className="nav-item ">
                            <NavLink className="nav-link " to="/">Home</NavLink>
                        </li>

                        {/* <li className="nav-item" id="correlation_search">
                            <NavLink className="nav-link " to="event-search">Correlation Searches</NavLink>
                        </li> */}
                        <li className="nav-item" id="neap">
                            <NavLink className="nav-link " to="neap">NEAP</NavLink>
                        </li>
                        {
                            AuthStatus === false &&
                            <li className="nav-item" id="login">
                                <NavLink className="nav-link " to="login">Login</NavLink>
                            </li>
                        }
                        {
                            AuthStatus === true &&
                            <li className="nav-item" id="login">
                                <Link className="nav-link" to="login" onClick={logoutHandler}>Logout</Link>
                            </li>
                        }

                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Navbar