import React from 'react';
function Selection(props) {
    // NEEd to optinmize on id coused Error
    return (
        <div>
            <select
                value={props.value}
                className={`form-select ${props.className}`}
                id={props.name}
                name={props.name}
                onChange={(e) => props.onSelectionChange(e, props.id, props.isAnd)}
            >
                {props.options.map((item, index) => {
                    return (
                        <option key={`unit--${index}`} value={item}>
                            {item}
                        </option>
                    );
                })}
            </select>
        </div>
    );
}

export default Selection;
