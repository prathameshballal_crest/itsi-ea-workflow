# C:\Program Files\Splunk\Python-3.7\Lib\site-packages\splunk\persistconn
from splunk.persistconn.application import PersistentServerConnectionApplication


from splunklib.client import connect


from utils import parse

import json
import sys


class DeleteNeapObj(PersistentServerConnectionApplication):
    def __init__(self, _command_line, _command_arg):
        super(PersistentServerConnectionApplication, self).__init__()

    # Handle a syncronous from splunkd.
    def handle(self, in_string):
        in_string_json = json.loads(in_string)

        from_parameter = in_string_json["query"][0][1]

        payload = from_parameter    

        opts = parse(sys.argv[1:], {}, ".splunkrc")
        opts.kwargs["username"] = "username"
        opts.kwargs["password"] = "password"
        opts.kwargs["owner"] = "Prathamesh Ballal, Nachiketa Bhoraniya, Rajdeep Kamai"
        opts.kwargs["app"] = "Sample-EA-workflow"
        try:
            service = connect(**opts.kwargs)
        except Exception as e:
            print("Error in connect")
            print(e)
        collection_name = "neap_object"
        # if the collection is found, print it out
        # if not, then create the collection
        if collection_name in service.kvstore:
            print("Collection %s found!" % collection_name)
        else:
            service.kvstore.create(collection_name)
        collection = service.kvstore[collection_name]
        # print out the data from the collection
        collection.data.delete(json.dumps({"_key": payload}))

        return {"payload": payload}
        # try:
        #     payload = json.load(payload)
        # except Exception as e:
        #     return {"payload": "error for json load", "error": e}
        # try:
        #     payload = payload[0]
        # except Exception as e:
        #     return {"payload": "error for 0", "error": e}

    def handleStream(self, handle, in_string):
        """
        For future use
        """
        raise NotImplementedError("PersistentServerConnectionApplication.handleStream")

    def done(self):
        """
        Virtual method which can be optionally overridden to receive a
        callback after the request completes.
        """
        pass
