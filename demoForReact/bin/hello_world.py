from splunk.persistconn.application import PersistentServerConnectionApplication

# C:\Program Files\Splunk\Python-3.7\Lib\site-packages\splunk\persistconn


class HelloWorld(PersistentServerConnectionApplication):
    def __init__(self, _command_line, _command_arg):
        super(PersistentServerConnectionApplication, self).__init__()

    # Handle a syncronous from splunkd.
    def handle(self, in_string):

        payload = {"text": "Hello world!"}
        return {"payload": payload, "status": 200}

    def handleStream(self, handle, in_string):
        """
        For future use
        """
        raise NotImplementedError("PersistentServerConnectionApplication.handleStream")

    def done(self):
        """
        Virtual method which can be optionally overridden to receive a
        callback after the request completes.
        """
        pass
