import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { AuthActions } from "../store";
function Login() {

    const [data, setData] = useState({ email: "", password: "" });
    const [error, setError] = useState({
        email: "",
        password: "",
        success: null,
    });
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        const authUser = JSON.parse(localStorage.getItem("authUser"));
        if (authUser) {
            if (authUser?.token === false || authUser?.token === null) {
                dispatch(AuthActions.login(authUser))
            }
        }
    }, [dispatch]);

    function changeHandler(event) {
        setError({ email: "", password: "", success: null }); // Resetting error when user is changing input.
        const name = event.target.name;
        const value = event.target.value;
        setData({ ...data, [name]: value });
    }

    function submitHandler(event) {
        event.preventDefault();

        if (data.email === "" || data.password === "") {
            setError({ ...error, password: "Please Enter all Fields." });
        } else if (data.password.length < 6) {
            setError({
                ...error,
                password: "Password must be at least 6 character.",
            });
        } else if (
            data.email === "admin@admin.com" &&
            data.password === "admin123"
        ) {
            dispatch(AuthActions.login(data)); // Dispatching Login event
            setError({ ...error, success: true });
            navigate("/"); // Redirecting to Home After login.
        } else {
            setError({ ...error, success: false });
            // setData({ email: "", password: "" });
        }
    }

    // For displaying error and Success
    function inputValidation() {
        if (error.success != null && error.success === true) {
            return <p className="text-success m-1">Login Successful.</p>;
        } else if (error.success != null && error.success === false) {
            return <p className="text-danger m-1">Email or Password is incorrect.</p>;
        } else {
            return "";
        }
    }

    return (
        <div
            className="container justify-content-center "
            onSubmit={submitHandler}
        >
            <form>
                <p className="text-center h3 text-white">Login</p>
                <div className="form-group row justify-content-center my-2">

                    <div className="col-md-4">

                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            className="form-control "
                            id="email"
                            name="email"
                            value={data.email}
                            placeholder="Enter email"
                            onChange={changeHandler}
                        />
                        {error.email !== "" && (
                            <p className="text-danger m-1">{error.email}</p>
                        )}
                    </div>
                </div>
                <div className="form-group row justify-content-center my-2">
                    <div className="col-md-4">

                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            id="password"
                            name="password"
                            value={data.password}
                            placeholder="Password"
                            onChange={changeHandler}
                        />
                        {error.password !== "" && (
                            <p className="text-danger m-1">{error.password}</p>
                        )}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-md-4">

                        <button
                            type="submit"
                            className="btn btn-primary form-control bg-shadow-color mt-2"
                        >
                            Login
                        </button>
                        {inputValidation()}
                    </div>
                </div>
            </form>
        </div>
    );
}

export default Login;
